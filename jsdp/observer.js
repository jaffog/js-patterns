/**
 * Observer
 *
 * Defines two base objects:
 *  Observer    used as an interface for objects acting as an observer,
 *              implementing the notify() method.
 *  Observable  used as an interface for objects that can be observed,
 *              implementing the methods registerObserver(),
 *              unregisterObserver(), and notifyObservers().
 *
 * @author      Michiel Bakker
 * @license     WTFPL (http://www.wtfpl.net)
 */
(function() {
"use strict";



}());